package com.redis.token;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * @author qhx
 * @date 2022/5/27
 */
public class ScheduleTest {

    public static void main(String[] args) {
        timerTest();
        try {
            Thread.currentThread().sleep(5000);
        } catch (InterruptedException e) {
        }
        System.out.println("main end");
    }

    //另起线程执行
    private static Timer time = new Timer(true);

    private static void timerTest(){
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    Thread.currentThread().sleep(3000);
                } catch (InterruptedException e) {
                }
                System.out.println("timerTest");
            }
        },1000);
    }

}
