package com.redis.token;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author qhx
 * @date 2022/1/6
 */
public class LockTest {

    private static Lock lock = new ReentrantLock(false);
    private static Condition condition =lock.newCondition();

    private static int num = 1;

    private static class Th implements Runnable{

        private static void lockMethod() throws InterruptedException {
            try {
                lock.lock();
                System.out.println(Thread.currentThread().getName()+ "lock");
                condition.await();
            } finally {
                lock.unlock();
            }
            System.out.println(Thread.currentThread().getName()+ "unlock");
        }

        @Override
        public void run() {
            try {
                lockMethod();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    private static class Th2 implements Runnable{

        private static void lockMethod() throws InterruptedException {
            try {
                lock.lock();
                System.out.println(Thread.currentThread().getName());
                condition.signalAll();
            } finally {
                lock.unlock();
            }
        }

        @Override
        public void run() {
            try {
                lockMethod();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new Th(),"thread1");
        Thread thread2 = new Thread(new Th(),"thread2");
        Thread thread3 = new Thread(new Th(),"thread3");
//WAITING
        thread1.start();thread2.start();thread3.start();
        TimeUnit.SECONDS.sleep(3);

        Thread thread = new Thread(new Th2(),"notify");
//        thread.start();
//        System.out.println(num);

    }

}
