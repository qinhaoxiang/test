package com.redis.token;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author qhx
 * @date 2022/5/28
 */
public class ThreadLocalTest {

    static ThreadLocal<Map<String,String>> threadLocal = new InheritableThreadLocal();

    public static void main(String[] args) {
        Map<String,String> m = new HashMap<>();
        m.put("1","1");
        threadLocal.set(m);
        Thread thread = new Thread(() -> {
            System.out.println(threadLocal.get());
            threadLocal.get().put("1","2");
            System.out.println("threadLocal changed");
        });
        thread.start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(threadLocal.get());
    }
}
