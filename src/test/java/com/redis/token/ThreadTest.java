package com.redis.token;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author qhx
 * @date 2021/12/14
 */
public class ThreadTest {
    private static ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 10, 10, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10));


    public static void main(String[] args) {
        //1、创建单个线程
        Thread thread = new Thread(new Task(), "task");
        thread.start();

        //2、线程池调度
        executor.execute(new Task());
        executor.execute(new Task());
        executor.execute(new Task());
        executor.execute(new Task());
        executor.execute(new Task());
        executor.execute(new Task());
        for (int i=0;i<10;i++){
            System.out.println(Thread.currentThread().getName()+i+Thread.currentThread().getThreadGroup());
        }
        System.out.println("线程池线程数量"+executor.getPoolSize());
//        executor.shutdown();
    }

    private static class Task implements Runnable{

        private int i = 0;

        @Override
        public void run() {
            synchronized (this){
            }
            for (int i=0;i<10;i++){
                System.out.println(i++ + "名称"+Thread.currentThread().getName()+";组别"+Thread.currentThread().getThreadGroup());
            }
        }
    }

}
