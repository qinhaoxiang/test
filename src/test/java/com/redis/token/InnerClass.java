package com.redis.token;

import java.util.AbstractCollection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author qhx
 * @date 2022/4/10
 */
public class InnerClass {
    private String s = "1";

    private static class Inner {
        private String ss = "a";

        private static String s = "1";
    }

    private  class Inner1 {
        private String ss = "a";

//        private static String s = "1";
    }

    private void init(){
        Runnable a = new Runnable(){

            @Override
            public void run() {
                System.out.println(s);
            }
        };
        System.out.println(Inner.s);
    }

    public static void main(String[] args) {
        LinkedList l = new LinkedList();
        ListIterator ll = l.listIterator(0);
        ll.nextIndex();
    }
}
class Test{

}
