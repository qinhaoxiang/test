package com.redis.token;

import com.redis.token.controller.TestController;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author qhx
 * @date 2022/5/30
 */
public class ClassTest {


    private String c ;
    public List<String> list;

    public static String name = "ClassTest";

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
//        test();
        ClassLoader c = ClassTest.class.getClassLoader();
        System.out.println(ClassLoader.getSystemClassLoader() == c);;
        genericTest();
//        construct();
//        annotationTest();
//        Field field = ClassTest.class.getField("name");
//        System.out.println(field.get(null));
    }

    public ClassTest(String c) {
        this.c = c;
    }

    private  static void annotationTest() {
        Class c = TestController.class;
        RestController a = (RestController)c.getAnnotation(RestController.class);
        a.getClass();
    }


    private static void genericTest() {
        Class c = ClassTest.class;
        try {
            Field field = c.getField("list");
            field.getGenericType();


        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private static void construct() {
        Constructor<ClassTest> c = null;
        try {
            c = ClassTest.class.getConstructor(String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        ClassTest cc = null;
        try {
            cc = c.newInstance("12");
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println(cc);
    }

    private static void test(){
        //对于数组，每种类型都有对应数组类型的Class对象
        String[] strings = new String[2];
        Object[] o = new Object[2];
        int[] ints = new int[2];
        Class c = strings.getClass();
        Class cc = o.getClass();
        Class ccc = ints.getClass();
        System.out.println(c == cc); //false
        System.out.println(c == ccc); //false
        System.out.println(ccc == cc); //false

        String[] string = new String[5];
        System.out.println(string.getClass() == c); //true


    }
}
