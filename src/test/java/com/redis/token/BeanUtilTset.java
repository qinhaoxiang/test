package com.redis.token;

import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qhx
 * @date 2021/12/29
 */
public class BeanUtilTset {

    static class Bean1{
        private String name = "Bean1";

        private List<Bean1> list;

        public List getList2() {
            return list2;
        }

        public void setList2(List list2) {
            this.list2 = list2;
        }

        private List list2;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Bean1> getList() {
            return list;
        }

        public void setList(List<Bean1> list) {
            this.list = list;
        }
    }

    static class Bean2{
        private String name;

        private List<Bean2> list;

        public List getList2() {
            return list2;
        }

        public void setList2(List list2) {
            this.list2 = list2;
        }

        private List list2;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Bean2> getList() {
            return list;
        }

        public void setList(List<Bean2> list) {
            this.list = list;
        }
    }

    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        Bean1 bean1 = new Bean1();
        List<Bean1> list = new ArrayList<>();
        list.add(new Bean1());
        bean1.setList(list);
        bean1.setList2(list);
        Bean2 obj = Bean2.class.newInstance();
        BeanUtils.copyProperties(bean1,obj);
        System.out.println(obj);

        List<Bean2> l = (List)list;
        obj.setList(l);
        Bean2 bean2 = obj.getList().get(0);
    }
}
