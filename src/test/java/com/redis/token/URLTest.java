package com.redis.token;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.CharsetUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

/**
 * @author qhx
 * @date 2022/3/1
 */
public class URLTest {


    public static void main(String[] args) {
        URL url;
        {
            try {
                char a = 'a';
                Character s = '秦';
                String q = "1";
                q.getBytes(StandardCharsets.UTF_8);


                url = new URL("https://www.baidu.com/");
                URLConnection urlConnection = url.openConnection();
                InputStream stream = urlConnection.getInputStream();
                String result = IoUtil.read(stream, CharsetUtil.UTF_8);
                System.out.println(result);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
