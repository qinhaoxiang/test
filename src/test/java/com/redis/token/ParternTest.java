package com.redis.token;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author qhx
 * @date 2022/6/6
 */
public class ParternTest {

    private static Pattern num = Pattern.compile("[0-9]{6}");

    private static Pattern space = Pattern.compile("[.0-9]+\\s*");

    private static Pattern str = Pattern.compile("(h){2,}");


    public static void main(String[] args) {
//        numTest();
        numTest1();
//          StrTest();
    }

    private static void StrTest() {
        String s = "xhxhxhhxhxhxhxhxhxhhxhxhxhxhxhxhxhhxhxhxhxhxhhxhx";
        Matcher matcher = str.matcher(s);
        while (matcher.find()){
            System.out.println(matcher.group());
        }
    }
    private static void numTest() {
        String s = "123456s123456";
        Matcher matcher = num.matcher(s);
        //matches 也会改变find的次数，执行一次matches相当于已经执行一次find了
//        boolean flag = matcher.matches();
//        System.out.println(flag);
        while (matcher.find()){
            System.out.println(matcher.group());
        }
    }

    private static void numTest1() {
        String s = "1.23 456  789 110";
        Matcher matcher = space.matcher(s);
        //matches 也会改变find的次数，执行一次matches相当于已经执行一次find了
//        boolean flag = matcher.matches();
//        System.out.println(flag);

//        while (matcher.find()){
        if(matcher.find())
            System.out.println(matcher.group());
//        }
    }
}
