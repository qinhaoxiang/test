package com.redis.token;

import com.redis.token.config.Knife4jConfiguration;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@SpringBootTest
class TokenApplicationTests {

//    @Before
//    void before() {
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Knife4jConfiguration.class);
//        context.getBeanFactory().registerSingleton("test",new Test1());
//        txService = context.getBean(Test1.class);
//    }

    @Test
    void contextLoads() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Knife4jConfiguration.class);
        context.getBeanFactory().registerSingleton("test",new Test1());
//        context.refresh();
//        context.register(Test1.class);
        Test1 t = context.getBean(Test1.class);
        t.test();
//        System.out.println(context.getBean(JdbcTemplate.class).queryForObject("select * from test2 where id in (0,1)", Map.class));
    }

}

class Test1 {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public void test() {
        jdbcTemplate.update("delete from test2 where id = 0");
        test1();
    }

    public void test1() {
        jdbcTemplate.update("delete from test2 where id = 1");
        throw new RuntimeException("");
    }


}
