package com.redis.token;

import java.util.concurrent.TimeUnit;

/**
 * @author qhx
 * @date 2022/1/5
 */
public class Demo06 {
    static Object object = new Object();
    static Object object2 = new Object();
    public static class T1 extends Thread {
        @Override
        public void run() {
            synchronized (object) {
                System.out.println(System.currentTimeMillis() + ":T1 start!");
                try {
                    System.out.println(System.currentTimeMillis() + ":T1 wait for object");
                    object.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(System.currentTimeMillis() + ":T1 end!");
            }
        }
    }

    public static class T3 extends Thread {
        @Override
        public void run() {
            synchronized (object) {
                System.out.println(System.currentTimeMillis() + ":T3 start!");
                try {
                    System.out.println(System.currentTimeMillis() + ":T3 wait for object");
                    object.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(System.currentTimeMillis() + ":T3 end!");
            }
        }
    }

    public static class T4 extends Thread {
        @Override
        public void run() {
            synchronized (object) {
                System.out.println(System.currentTimeMillis() + ":T4 start，notify one thread! ");
                object.notify();
                System.out.println(System.currentTimeMillis() + ":T4 end!");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static class T2 extends Thread {
        @Override
        public void run() {
            synchronized (object) {
                System.out.println(System.currentTimeMillis() + ":T2 start，notify one thread! ");
                object.notify();
                System.out.println(System.currentTimeMillis() + ":T2 end!");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class R1 implements Runnable {
        @Override
        public void run() {
            System.out.println("threadName:" + Thread.currentThread().getName());
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static class T extends Thread {
        @Override
        public void run() {
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (object){
                System.out.println("t-start");
                object.notify();
                System.out.println("t-end");
            }
//            while (true) {
//                //循环处理业务
//                if (this.isInterrupted()) {
//                    break;
//                }
//                System.out.println(Thread.currentThread().getName());
//            }
        }
    }
    public static void main(String[] args) throws InterruptedException {
        new T3().start();
        new T4().start();


//        ThreadGroup threadGroup = new ThreadGroup("thread-group-1");
//        Thread t1 = new Thread(threadGroup, new R1(), "t1");
//        Thread t2 = new Thread(threadGroup, new R1(), "t2");
//        t1.start();
//        t2.start();
//        TimeUnit.SECONDS.sleep(1);
//        System.out.println("活动线程数:" + threadGroup.activeCount());
//        System.out.println("活动线程组:" + threadGroup.activeGroupCount());
//        System.out.println("线程组名称:" + threadGroup.getName());
//        System.out.println("main:" + Thread.currentThread().getThreadGroup().getName()+Thread.currentThread().getThreadGroup().activeCount());
//        Thread.currentThread().getThreadGroup().list();

//        T t = new T();
//        t.start();
////        TimeUnit.SECONDS.sleep(3);
////        t.interrupt();
//        synchronized (object){
//            System.out.println("main-start");
//            //wait 执行完会释放锁
//            object.wait();
//
//            //执行下面的要重新获取锁
//            TimeUnit.SECONDS.sleep(3);
//            System.out.println("main-end");
//        }

    }
}
