package com.redis.token;

/**
 * @author qhx
 * @date 2021/12/14
 */
public class StringPololTest {

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public  void run(){
                System.out.println("jvm停止");
            };
        });


        //运行时常量池相对于Class文件常量池的另外一个重要特征是具备动态性，Java语言并不要求常量一定只有编译期才能产生(不过一般情况下确实只有编译期间产生)，
        // 也就是说，并非预置入Class文件中常量池的内容才能进入方法区运行时常量池，运行期间也可以将新的常量放入池中，
        // 这种特性被开发人员利用得比较多的便是String类的intern()方法

        String s1 = new String("aasafafaf");
        String s = "34525235fsdgsdg";
        System.out.println(s1==s);
        byte a = 16;
        int b =  +a;
        System.out.println(b);
    }
}
