package com.redis.token;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author qhx
 * @date 2022/1/8
 */
public class DeadLockTest {

    private static Lock lock = new ReentrantLock();

    private static Lock lock1 = new ReentrantLock();

    private static Object obj = new Object();

    private static Object obj1 = new Object();

    public static void main(String[] args) throws InterruptedException {
        //deadLock example 1
//        Thread thread = new Thread(()->{
//            lock.lock();
//            System.out.println("thread获取到锁lock");
//            try {
//                TimeUnit.SECONDS.sleep(3);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            try {
//                lock1.lock();
//                System.out.println("thread获取到锁lock1");
//            } finally {
//                lock.unlock();
//                lock1.unlock();
//            }
//        });
//        thread.setName("thread");
//        thread.start();
//        Thread thread1 = new Thread(()->{
//            try {
//                lock1.lock();
//                System.out.println("thread1获取到锁lock1");
//                lock.lock();
//                System.out.println("thread1获取到锁lock");
//            } finally {
//                lock.unlock();
//                lock1.unlock();
//            }
//        });
//        thread1.setName("thread1");
//        thread1.start();


        //deadLock example 2
        Thread thread2 = new Thread(()->{
           synchronized (obj){
               try {
                   System.out.println("thread2获取到锁obj");
                   TimeUnit.SECONDS.sleep(5);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               synchronized(obj1){
                   System.out.println("thread2获取到锁obj1");
               }
           }
        });
        thread2.setName("thread2");
        thread2.start();{
            TimeUnit.SECONDS.sleep(1);
        }
        synchronized (obj1){
                System.out.println("main获取到锁obj1");
            synchronized(obj){
                System.out.println("main获取到锁obj");
            }
        }
    }
}
