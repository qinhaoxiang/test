package com.redis.token;

import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.Annotation;
import java.util.Arrays;

/**
 * @author qhx
 * @date 2022/3/20
 */
public class Class_getAnnotation_Test {

    public static void main(String[] args) {
        getAnnotation();
    }

    private static void getAnnotation(){
        Class clazz = Controller.class;
        RequestMapping request = (RequestMapping) clazz.getAnnotation(RequestMapping.class);
        System.out.println(Arrays.toString(request.value()));
    }

    @RequestMapping("123")
    class Controller{

        @RequestMapping("456")
        public void map(){}

    }
}
