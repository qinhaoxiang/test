package com.redis.token;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qhx
 * @date 2022/1/4
 */
public class GenericTest<T> {

    private T t;

    public <E> void show_3(E e,T t) {
        System.out.println(e.toString());
        t.toString();
    }

//    public /*<E>*/ void show_4(E e) {
//        System.out.println(e.toString());
//    }

    public static void test(GenericTest<String> test) {
        //编译错误
//        List<String>[] ls = new ArrayList<String>[10];
        List<String>[] ls = new ArrayList[10];
    }

    public void  test1(){
        List<Object> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        List<?> list3 = new ArrayList<>();

        List<?> list = new ArrayList<>();
        //编译错误
//        list.add("12");

    }

    public static <E extends String & List> void test3(E t) {
        System.out.println(t.getClass());
    }
    public static <E>  void createInstance(Class<E> clazz) throws IllegalAccessException, InstantiationException {
//        return clazz.newInstance();
    }


    public static void main(String[] args) {
        GenericTest<String> test = new GenericTest<>();
        synchronized (GenericTest.class){
            try {
                test.wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//        test.test1();
//        GenericTest.test3("12");
    }
}
