package com.redis.token.annotation;

import com.redis.token.annotation.validator.CustomValidator;
import org.springframework.core.annotation.AliasFor;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author qhx
 * @date 2021/12/8
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CustomValidator.class)
public @interface CustomValid {
    @AliasFor("value")
    String message() default "default message";

    @AliasFor("message")
    String value() default "";
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
