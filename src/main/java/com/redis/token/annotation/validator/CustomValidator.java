package com.redis.token.annotation.validator;

import com.redis.token.annotation.CustomValid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author qhx
 * @date 2021/12/8
 */
public class CustomValidator implements ConstraintValidator<CustomValid, String> {

    @Override
    public void initialize(CustomValid constraintAnnotation) {
        System.out.println("init CustomValidator");
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null) {
            throw new RuntimeException("参数不能为空");
        }
        if (s.startsWith("love")) {
            return true;
        }
        return false;
    }
}
