package com.redis.token.controller;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author qhx
 * @date 2022/6/24
 */
public class SqlSession {
    private static SqlSessionFactory sqlSessionFactory = build();
    public static SqlSessionFactory build() {
        try {
            return new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-config.xml"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void test() {
        //true自动提交事务
        List<Map<String, Object>> l =  sqlSessionFactory.openSession(true).getMapper(TestMapper.class).get();
        l.get(0);
    }
}
