package com.redis.token.controller;


import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author qhx
 * @date 2022/6/22
 */
@Mapper
public interface TestMapper {

    List<Map<String, Object>> get();

    boolean set(TestEntity testEntity);
}
