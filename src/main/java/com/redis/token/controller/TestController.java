package com.redis.token.controller;

import cn.hutool.core.bean.BeanUtil;
import com.redis.token.bean.BeanFactoryPostProcessorBean;
import com.redis.token.bean.CustomValidateBean;
import com.redis.token.config.proT;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author qhx
 * @date 2021/12/7
 */
@Controller
public class TestController implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Autowired
    private TestMapper testMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/test")
    @CrossOrigin(origins = "*")
    @ResponseBody
    public String test() {
        Object myBean = applicationContext.getBean("myBean");
        String name = ((BeanFactoryPostProcessorBean) myBean).getName();
        //动态注册bean
        ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext) applicationContext;
        ConfigurableListableBeanFactory configurableListableBeanFactory = configurableApplicationContext.getBeanFactory();
        configurableListableBeanFactory.registerSingleton("registerBean", "123");

         return "123";
//        throw new RuntimeException("eeeeeeeeeeeeeeeeee");
    }

    @GetMapping("/redirect")
    public void redirect(HttpServletResponse response) {
        try {
            response.sendRedirect("https://www.baidu.com/");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/forward")
    public String forward(HttpServletResponse response) {
            return "forward:/index.html";

    }


    @PostMapping("/valid")
    public Object Validate(@Validated @RequestBody CustomValidateBean validateBean) {
        System.out.println("当前线程"+Thread.currentThread().getName());
        return validateBean;
    }

    @GetMapping("/mysql")
    @ResponseBody
//    @Transactional(rollbackFor = Exception.class)
    public String mysql() {
        List<Map<String, Object>> result = testMapper.get();
        List<Map<String, Object>> ll =  jdbcTemplate.queryForList("select * from test1 t where t.id between 0 and 1500");
        List<TestEntity> l = ll.stream().map(e->transfer(e,TestEntity.class)).collect(Collectors.toList());
        TestEntity t = new TestEntity();
        t.setId(20000001);t.setEmail("2022@163.com");t.setName("2022");t.setSex(((byte)0));
//        List<Object[]> batchArgs  = new ArrayList<>(200000);
//        for (int i = 0; i < 20000; i++) {
//            batchArgs.add(new String[]{(2000000 + i) + "","batch" + i,"1","batch@163.com"});
//        }
        List<TestEntity> list = new ArrayList<>(20000);
        for (int i = 0; i < 20000; i++) {
            TestEntity testEntity = new TestEntity();
            testEntity.setId(i);
            testEntity.setName("123");
            testEntity.setSex((byte)0);
            testEntity.setEmail("123@qq.com");
            list.add(testEntity);
        }
        long tt = System.currentTimeMillis();
        jdbcTemplate.batchUpdate("insert into test2 values (?,?,?,?)", new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                preparedStatement.setLong(1,list.get(i).getId());
                preparedStatement.setString(2,list.get(i).getName());
                preparedStatement.setString(3,list.get(i).getSex()+ "");
                preparedStatement.setString(4,list.get(i).getEmail());
            }

            @Override
            public int getBatchSize() {
                return 20000;
            }
        });
//        int[] ints = jdbcTemplate.batchUpdate("insert into test2 values (?,?,?,?)",batchArgs);
        System.out.println((System.currentTimeMillis() - tt));
//        boolean b = testMapper.set(t);
//        throw new RuntimeException();
        return "result";
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    private <T> T transfer(Map<String, Object> map, Class<T> entity) {
        T t = null;
        try {
            t = entity.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        BeanUtil.copyProperties(map, t);
        return t;
    }
    private class Itr{
        void test(){
            Object o = applicationContext;
        }
    }
}
