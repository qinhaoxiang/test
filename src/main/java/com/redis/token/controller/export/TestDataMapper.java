package com.redis.token.controller.export;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author qhx
 * @date 2022/6/22
 */
public interface TestDataMapper extends BaseMapper<TestData> {

}
