package com.redis.token.controller.export;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qhx
 * @date 2024/8/15
 */
public class SheetMember<T> {

    private static int defaultSize = 100000;

    private ArrayList<T> member;

    public SheetMember() {
        this.member = new ArrayList<>(defaultSize);
    }

    public void initMember() {
        this.member = new ArrayList<>(defaultSize);
    }

    public void add(T t) {
        this.member.add(t);
    }

    public boolean isFull() {
        return this.member.size() == defaultSize;
    }

    public List<T> toList() {
        return this.member;
    }

    public static int getSize() {
        return defaultSize;
    }
}
