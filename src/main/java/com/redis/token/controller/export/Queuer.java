package com.redis.token.controller.export;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author qhx
 * @date 2024/8/15
 */
public class Queuer<T> {

    private boolean end = false;
    private LinkedBlockingQueue<List<T>> queue = new LinkedBlockingQueue<>(10);

    public boolean isEnd() {
        return end;
    }

    public void put(List<T> data) {
        try {
            queue.offer(data, 3, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            this.end = true;
            throw new RuntimeException("queue full");
        }
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
    public List<T> get() {
        try {
            return queue.poll(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            this.end = true;
            throw new RuntimeException("queue empty");
        }
    }

    public void setEnd(boolean end) {
        this.end = end;
    }
}
