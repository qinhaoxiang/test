package com.redis.token.controller.export;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author qhx
 * @date 2024/8/15
 */
@TableName(value = "elec_meter")
public class TestData {

    @TableField("id")
    private Integer id;

    @TableField("ASSET_NO")
    private String assetNo;

    @TableField("COMM_ADDR")
    private String commAddr;

    @TableField("MGT_ORG_CODE")
    private String mgtOrgCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public String getCommAddr() {
        return commAddr;
    }

    public void setCommAddr(String commAddr) {
        this.commAddr = commAddr;
    }

    public String getMgtOrgCode() {
        return mgtOrgCode;
    }

    public void setMgtOrgCode(String mgtOrgCode) {
        this.mgtOrgCode = mgtOrgCode;
    }
}
