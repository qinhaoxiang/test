package com.redis.token.controller.export;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.metadata.WriteSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author qhx
 * @date 2024/8/15
 */
@RestController
public class BigDataExportController {

    @Autowired
    private TestDataMapper baseMapper;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private ThreadPoolExecutor executor = new ThreadPoolExecutor(10, 10, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100));

    @GetMapping("/export")
    public void export() {
        // 从数据库获取表所有记录，做数据处理
        Queuer<TestData> queuer = new Queuer<>();
        SheetMember<TestData> sheetMember = new SheetMember<>();

        //内存最大缓存数
        writeExcel("test.xlsx", 1000000, queuer);
        System.out.println("数据读取开始");

        jdbcTemplate.query(con -> {
            con.setAutoCommit(false);
            PreparedStatement preparedStatement =
                    con.prepareStatement("select * from elec_meter",
                            ResultSet.TYPE_FORWARD_ONLY,
                            ResultSet.CONCUR_READ_ONLY);
            preparedStatement.setFetchSize(Integer.MIN_VALUE);
            preparedStatement.setFetchDirection(ResultSet.FETCH_FORWARD);
            return preparedStatement;
        }, result -> {
            TestData h2User = new TestData();
            h2User.setId(result.getInt("id"));
            h2User.setAssetNo(result.getString("ASSET_NO"));
            h2User.setCommAddr(result.getString("COMM_ADDR"));
            h2User.setMgtOrgCode(result.getString("MGT_ORG_CODE"));
            sheetMember.add(h2User);
            if (sheetMember.isFull()) {
                queuer.put(sheetMember.toList());
                sheetMember.initMember();
            }
        });
        queuer.put(sheetMember.toList());
        queuer.setEnd(true);
        System.out.println("数据读取完成");
    }

    public <T> void writeExcel(String fileName, int sheetSize, Queuer<T> queuer) {
        executor.execute(() -> {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            int batchNo = 1;
            int sheetNo = 1;
            File dirFile = new File("/data1/files");
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            File file = new File(dirFile, fileName);
            ExcelWriter writer = EasyExcel.write(file, TestData.class).excelType(ExcelTypeEnum.XLSX).build();
            WriteSheet sheet = getSheet(writer, sheetNo);
            while (!queuer.isEnd() || !queuer.isEmpty()) {
                List<T> member = queuer.get();
                if (Objects.nonNull(member)) {
                    writer.write(member, sheet);
                    System.out.println("批次-" + batchNo + "-导出成功");
                    if ((++batchNo * SheetMember.getSize() % sheetSize == 0)) {
                        sheet = getSheet(writer, ++sheetNo);
                    }
                }
            }
            stopWatch.stop();
            writer.close();
            System.out.println(fileName + "导出成功,耗时" + stopWatch.getTotalTimeMillis() + "ms");
        });
    }

    private WriteSheet getSheet(ExcelWriter writer, int sheetNo) {
        ExcelWriterSheetBuilder builder = new ExcelWriterSheetBuilder(writer);
        builder.sheetNo(sheetNo);
        return builder.build();
    }

}
