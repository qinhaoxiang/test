package com.redis.token.beanDefinitionRegistryPostProcessorTest;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.Configuration;

/**
 * @author qhx
 * @date 2021/12/8
 * BeanDefinitionRegistryPostProcessor在BeanFactoryPostProcessor#postProcessBeanFactory之前调用。
 * 它更关注于bean的定义，BeanFactoryPostProcessor更适合于查找已完成的定义再修改，当然也可以用于新增定义。
 */
@Configuration
public class BeanDefinitionRegistryPostProcessorTest implements BeanDefinitionRegistryPostProcessor {
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
        System.out.println("BeanDefinitionRegistryPostProcessor的postProcessBeanDefinitionRegistry方法");
//        beanDefinitionRegistry.registerBeanDefinition();
    }


    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        System.out.println("BeanDefinitionRegistryPostProcessor的postProcessBeanFactory方法");
    }
}
