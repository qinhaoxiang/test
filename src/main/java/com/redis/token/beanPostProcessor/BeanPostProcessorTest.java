package com.redis.token.beanPostProcessor;

import com.redis.token.bean.BeanFactoryPostProcessorBean;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;


/**
 * @author qhx
 * @date 2021/12/7
 * BeanPostProcessor接口回调对应的主体是bean，其可以在bean实例化以后但是在调用其初始化方法前后进行回调以达到对bean进行处理的效果。每个bean实例化后调一次
 */
@Component
public class BeanPostProcessorTest implements BeanPostProcessor {

    /*两个方法的参数以及返回值对应的意义都是一样的，其中参数bean表示当前状态的bean，参数beanName表示当前bean的名称，
    而方法对应的返回值即表示需要放入到bean容器中的bean，所以用户如果有需要完全可以在这两个方法中对bean进行修改，即封装自己的bean进行返回*/




    /*方法postProcessBeforeInitialization()将在一个bean被完全初始化前进行回调，
      此时对应的bean已经实例化了，但是对应的属性注入等还没有进行，
      即在调用InitializingBean的afterPropertiesSet()方法或bean对应的init-method之前*/
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
//        System.out.println("-------进入postProcessBeforeInitialization方法");
        //为BeanFactoryPostProcessorBean对象生成代理bean
        if (bean.getClass() == BeanFactoryPostProcessorBean.class) {
            ProxyFactory proxy = new ProxyFactory();
            proxy.setTarget(bean);
            InterceptorTest interceptorTest = new InterceptorTest();
            interceptorTest.setTarget(bean);
            proxy.addAdvice(interceptorTest);
            return proxy.getProxy();
        }
        return bean;
    }

    /*将在bean被完全初始化后进行回调，此时对应的依赖注入已经完成，即在调用InitializingBean的afterPropertiesSet()方法或对应init-method方法之后*/
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

//    public <T, V> T genericMethod_test02(T generic01, V value) {
//        System.out.println("我是genericMethod_test02");
//        return generic01;
//    }

    public BeanPostProcessorTest() {
        System.out.println("BeanPostProcessor构造方法调用");
    }

    private class InterceptorTest implements MethodInterceptor {
        public void setTarget(Object target) {
            this.target = target;
        }

        private Object target;

        @Override
        public Object invoke(MethodInvocation methodInvocation) throws Throwable {
            System.out.println("进入代理");
            return methodInvocation.getMethod().invoke(target);
        }
    }
}
