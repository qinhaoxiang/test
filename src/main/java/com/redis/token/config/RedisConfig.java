package com.redis.token.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author qhx
 * @date 2021/8/24
 */
@Component
public class RedisConfig implements InitializingBean {

    @Autowired
    RedisConnectionFactory redisConnectionFactory;

    @PostConstruct
/*    个人测试之后理解：对于没有循环依赖的情况，实例化一个bean的时候，默认空参构造，
    然后注入依赖。遇到需要注入的依赖，去实例化此依赖，然后注入。所以可以对需注入的依赖
    实现InitializingBean 接口，完成实例化且属性注入完成之后的操作，修改该依赖。*/

    // PostConstruct在类构造结束、注入结束执行，ApplicationRunner在应用启动后执行
    public void save(){
        System.out.println("-------------------@PostConstruct");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("-------------------afterPropertiesSet");
    }
}
