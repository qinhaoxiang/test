package com.redis.token.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * @author qhx
 * @date 2021/8/25
 */
@Component
public class InitializingBeanTest implements InitializingBean {
    /*InitializingBean是用来定义ApplicationContext在完全初始化一个bean以后需要需要回调的方法的，
    其中只定义了一个afterPropertiesSet()方法。如其名称所描述的那样，该方法将在ApplicationContext将一个bean完全初始化，
    包括将对应的依赖项都注入以后才会被调用*/
    @Override
    public void afterPropertiesSet() throws Exception {
//        System.out.println("-----------------afterPropertiesSet");
    }
}
