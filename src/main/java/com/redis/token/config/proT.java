package com.redis.token.config;

import com.redis.token.pro.pro;
import org.junit.Test;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author qhx
 * @date 2021/10/22
 */
public class proT extends pro{

    @Test
    public void contextLoads() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Knife4jConfiguration.class);
        Knife4jConfiguration.Test1 t = context.getBean(Knife4jConfiguration.Test1.class);
        t.test();

        System.out.println(context.getBean(JdbcTemplate.class).queryForObject("select * from test2 where id in (0,1)", Map.class));
    }

    public void test() {
        pro pro = new pro();
        String s = this.get();
//        pro.get();
    }

    private static class testBeanWapper{
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getList() {
            return list;
        }

        public void setList(List<String> list) {
            this.list = list;
        }

        public String[] getStrings() {
            return strings;
        }

        public void setStrings(String[] strings) {
            this.strings = strings;
        }

        private String name = "testBeanWapper";

        private List<String> list = new ArrayList<>();

        private String [] strings = new String[8];

    }

    public static void main(String[] args) {
        testBeanWapper t = new testBeanWapper();
        BeanWrapper beanWrapper = new BeanWrapperImpl(t);
        String s = (String) beanWrapper.getPropertyValue("name");
       boolean flag = beanWrapper.getWrappedInstance() == t;  //true
        beanWrapper.getWrappedClass();
//        beanWrapper.setPropertyValue();
    }
}
