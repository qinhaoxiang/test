package com.redis.token.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * @author qhx
 * @date 2021/8/25
 */
@Component
public class ApplicationTest implements ApplicationRunner {

    public static List<Object> list(){
        List<Object> ll = new ArrayList<>();
        ll.add("123");
        return ll;
    }

    @Autowired
    RedisConnectionFactory redisConnectionFactory;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("--------------------ApplicationRunner"+redisConnectionFactory.toString());
    }

}
