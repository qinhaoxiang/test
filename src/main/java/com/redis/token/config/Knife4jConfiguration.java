package com.redis.token.config;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @author qhx
 * @date 2022/6/17
 */
@EnableSwagger2WebMvc
@EnableTransactionManagement //开启spring事务管理功能
@Configuration //指定当前类是一个spring配置类
@ComponentScan //开启bean扫描注册
public class Knife4jConfiguration {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        //.title("swagger-bootstrap-ui-demo RESTful APIs")
                        .description("# swagger-bootstrap-ui-demo RESTful APIs")
                        .termsOfServiceUrl("http://www.xx.com/")
                        .contact("xx@qq.com")
                        .version("1.0")
                        .build())
                //分组名称
                .groupName("2.X版本")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.github.xiaoymin.knife4j.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

    /*
    * 自己创建了DataSourceBean，DataSourceConfiguration.Hikari 的HikariDataSource就不会自己创建了
    * @ConfigurationProperties(prefix="spring.datasource.hikari")，会将spring.datasource.hikari配置，逐个属性赋值给DataSource 。
    * */
    @Bean
    public DataSource dataSource() throws Exception {
        DataSourceProperties properties = new DataSourceProperties();
        properties.setUrl("jdbc:mysql://localhost:3306/javacode2018_employees?characterEncoding=UTF-8&rewriteBatchedStatements=true");
        properties.setUsername("root");
        properties.setPassword("root");
        properties.setDriverClassName("com.mysql.jdbc.Driver");
        properties.afterPropertiesSet();
        DataSource dataSource = properties.initializeDataSourceBuilder().build();
        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
    //定义一个事务管理器
    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    public Test1 test1(JdbcTemplate jdbcTemplate) {
        return new Test1(jdbcTemplate);
    }


//    @Test
//    public void contextLoads() {
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Knife4jConfiguration.class);
//        Test1 t = context.getBean(Test1.class);
//        t.test();
//
//        System.out.println(context.getBean(JdbcTemplate.class).queryForObject("select * from test2 where id in (0,1)", Map.class));
//    }
    public class Test1 {
        @Autowired
        private JdbcTemplate jdbcTemplate;

        public Test1(JdbcTemplate jdbcTemplate) {
            this.jdbcTemplate = jdbcTemplate;
        }

        @Transactional
        public void test() {
            jdbcTemplate.update("delete from test2 where id = 0");
            test1();
        }

        public void test1() {
            jdbcTemplate.update("delete from test2 where id = 1");
            throw new RuntimeException("");
        }


    }
}
