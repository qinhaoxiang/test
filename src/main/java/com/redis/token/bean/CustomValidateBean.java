package com.redis.token.bean;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.redis.token.annotation.CustomValid;

import javax.validation.constraints.NotBlank;

/**
 * @author qhx
 * @date 2021/12/8
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY) //增加该注解，就算没有public的get、set方法也能扫描到对应的field
public class CustomValidateBean {

    @CustomValid("name need love plz")
    private String name;

    private Integer age;

}
