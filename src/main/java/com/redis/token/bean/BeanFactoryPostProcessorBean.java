package com.redis.token.bean;

/**
 * @author qhx
 * @date 2021/12/7
 */
public class BeanFactoryPostProcessorBean {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
