package com.redis.token;

//import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

@SpringBootApplication
//@MapperScan("com.redis.token")
//@EnableDiscoveryClient
public class TokenApplication implements EnvironmentAware {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(TokenApplication.class);
        //事件监听
        springApplication.addListeners(new ApplicationListener<ApplicationStartingEvent>() {
            @Override
            public void onApplicationEvent(ApplicationStartingEvent testEvent) {
                System.out.println("-------------------" + System.getProperty("user.home") + "TokenApplication running testEvent!");
            }
        });
        springApplication.run(args);
    }

    @Override
    public void setEnvironment(Environment environment) {
        String s = ((ConfigurableEnvironment) environment).getProperty("server.port");
        System.out.println("=====================" + s);
    }

    private static class TestEvent extends ApplicationEvent {
        public TestEvent(Object source) {
            super(source);
        }
    }

}
