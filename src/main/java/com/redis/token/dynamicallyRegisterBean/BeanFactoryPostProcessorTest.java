package com.redis.token.dynamicallyRegisterBean;

import com.redis.token.bean.BeanFactoryPostProcessorBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author qhx
 * @date 2021/12/7
 * BeanFactoryPostProcessor会在ApplicationContext内部的BeanFactory加载完bean的定义后，但是在对应的bean实例化之前进行回调
 * Spring能够自动检测到定义在bean容器中BeanFactoryPostProcessor对应的bean，
 * ！并将在所有其它bean定义进行实例化之前对它们进行实例化！
 * 之后再回调其中的postProcessBeanFactory()方法。    尚未实例化任何 bean
 */
@Component
public class BeanFactoryPostProcessorTest implements BeanFactoryPostProcessor, EnvironmentAware {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        System.out.println("postProcessBeanFactory");
        GenericBeanDefinition bd = new GenericBeanDefinition();
        bd.setBeanClass(BeanFactoryPostProcessorBean.class);
        bd.getPropertyValues().add("name", "BeanFactoryPostProcessor");
        ((DefaultListableBeanFactory) configurableListableBeanFactory).registerBeanDefinition("myBean", bd);
    }

    @Override
    public void setEnvironment(Environment environment) {
        System.out.println("进入setEnvironment方法");
    }

    public BeanFactoryPostProcessorTest() {
        System.out.println("BeanFactoryPostProcessor构造方法调用");
    }
}
